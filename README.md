# List of open speech Mastodon instances

**This is a list of Mastodon instances whose admins value BOTH diversity of speech AND courteous behavior.**

These instances federate widely with others and block or silence few or none, while still moderating their own users in a common sense fashion. Because these instances do require common courtesy from their users, we expect that many of the "safe speech" instances will federate with them.

If your priority as a user is to be able to talk to most or all other users in the fediverse - the network which includes but is not limited to Mastodon - you may wish to join one of these instances.

For more information on why instances may or may not be featured on this list, see below. If you have a question or comment, toot at me at @deutrino@mstdn.io :)


## The list

Presented in alphabetical order. This list was launched in December 2017 by me trolling through the rules pages of every instance with over 2000 toots. Once it's changed significantly from that, I'll remove this caveat.

* chaos.social
* equestria.social
* freeradical.zone
* i.write.codethat.sucks
* maly.io
* mastodon.at
* mastodon.org.uk
* metalhead.club
* mstdn.io
* octodon.social
* tenforward.social


# Criteria for inclusion

To be on this list (and stay on it), instance admins should do the following:

* "silence" block other instances rarely
* "suspend" block other instances very, very rarely, if at all
* maintain a public list of blocked instances
* do not allow NSFW content without CWs / tags
* do not allow harassment / brigading / obvious trolling / doxing, etc
* deal with their own users swiftly and in good faith if they cause problems for others


# FAQ

## What is "open speech?"

**Open speech** is a term I made up :)

It means valuing diversity of ideas enough to tolerate speech which some, or even many, find objectionable - _if_ that speech is presented without intent to actively harass or annoy others.

## What do you mean by "harassment?"

I mean harassment with intrusive intent. For the purposes of this list, posting content which many may find objectionable, but in a way which is unlikely to intrude on their experience in the fediverse, is _not_ considered harassment.

A brief example: posting in support of (for example) legislation which limits bathroom access for transgender people is _not_ considered harassment in this context; however, doing so and mentioning LGBT+ users who were uninvolved in prior discussion with the poster, could be considered harassment, particularly if it is repetitive.

## What about hate speech?

Instances on this list are free to ban hate speech explicitly, or to handle it on a case by case basis. An explicit blanket ban on hate speech is not a requirement to be on this list. However, it may be a good idea to take measures to keep hate speech out of the public timelines, e.g. by enforcing a policy of good-faith CWs or by silencing particular users.

This is a pragmatic list, not an idealistic one. If enough instances with moderate policies start blocking an instance on this list, it is likely to be removed. The goal here is to provide a list of instances whose users can receive _and send_ to as many others as possible.

## What if users on an instance listed here are causing problems?

In the event of complaints about their users, this will require good faith judgment on the part of admins. This is also true when it comes to administrating this list itself. Inclusion on the list will always be subjective to some degree. If an instance on the list has repeatedly been a source of problems to the point where other instances with fairly moderate blocking policies are silencing or suspending it, it will be removed.

